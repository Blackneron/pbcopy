; ======================================================================
;
;                         PBcopy - Robocopy GUI
;
;                              Version 1.2
;
;               Copyright 2015 / PB-Soft / Patrick Biegel
;
;                            www.pb-soft.com
;
; ======================================================================

; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   S E T T I N G S
; ======================================================================

; Enable autotrim.
AutoTrim, On

; Specify that only one instance of this application can run.
#SingleInstance ignore

; Set the script speed.
SetBatchLines, 20ms

; Hide the script and don't display a tray icon.
#NoTrayIcon

; Set the working directory.
SetWorkingDir, %A_ScriptDir%


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   I N F O R M A T I O N
; ======================================================================

; Specify the application name.
ApplName = PBcopy

; Specify the application version.
ApplVersion = 1.2

; Specify the internal application version.
ApplIntVersion = 44

; Calculate the application modification time.
FileGetTime, ModificationTime, PBcopy.exe

; Format the application build date.
FormatTime, ApplBuildDate , %ModificationTime%, dd.MM.yyyy / HH:mm:ss

; Specify the application website.
ApplWebsite = http://pb-soft.com

; Specify the copyright text.
Copyright := "Copyright " . Chr(169) . " " . A_YYYY . " - PB-Soft"


; ======================================================================
; I N I T I A L I Z E   V A R I A B L E S
; ======================================================================

; Specify the configuration file.
ConfigurationFile = %A_ScriptDir%\config\PBcopy.ini

; Specify the logfile.
LogFile = %A_ScriptDir%\log\PBcopy.log


; ======================================================================
; C R E A T E   D I R E C T O R I E S
; ======================================================================

; Check if the configuration directory does not exist.
IfNotExist, %A_ScriptDir%\config
{

  ; Create the configuration directory.
  FileCreateDir, %A_ScriptDir%\config

} ; Check if the configuration directory does not exist.

; Check if the image directory does not exist.
IfNotExist, %A_ScriptDir%\image
{

  ; Create the image directory.
  FileCreateDir, %A_ScriptDir%\image

} ; Check if the image directory does not exist.

; Check if the log directory does not exist.
IfNotExist, %A_ScriptDir%\log
{

  ; Create the log directory.
  FileCreateDir, %A_ScriptDir%\log

} ; Check if the log directory does not exist.


; ======================================================================
; F I L E   I N S T A L L A T I O N
; ======================================================================

; Install the configuration file.
FileInstall, config\PBcopy.ini, config\PBcopy.ini, 0

; Install the graphic files.
FileInstall, image\Logo.gif, image\Logo.gif, 0
FileInstall, image\PBcopy.jpg, image\PBcopy.jpg, 0


; ======================================================================
; C H E C K   I F   T H E   F I L E S   E X I S T
; ======================================================================

; Check if the configuration file does not exist.
IfNotExist, %ConfigurationFile%
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, The configuration 'PBcopy.ini' does not exist !

  ; Exit the application.
  ExitApp

} ; Check if the configuration file does not exist.

; Check if the logo does not exist.
IfNotExist, %A_ScriptDir%\image\Logo.gif
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, The logo 'Logo.gif' does not exist !

  ; Exit the application.
  ExitApp

} ; Check if the logo does not exist.

; Check if the background picture does not exist.
IfNotExist, %A_ScriptDir%\image\PBcopy.jpg
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, The background picture 'PBcopy.jpg' does not exist !

  ; Exit the application.
  ExitApp

} ; Check if the background picture does not exist.

; Check if the Robocopy application does not exist.
IfNotExist, %A_ScriptDir%\Robocopy.exe
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, The backup application 'Robocopy.exe' does not exist !

  ; Exit the application.
  ExitApp

} ; Check if the Robocopy application does not exist.


; ======================================================================
; R E A D   C O N F I G U R A T I O N
; ======================================================================

; Read the path of the source path.
IniRead, SourcePath, %ConfigurationFile%, Configuration, SourcePath

; Read the path of the destination path.
IniRead, DestinationPath, %ConfigurationFile%, Configuration, DestinationPath

; Read the mirror setting.
IniRead, MirrorBackup, %ConfigurationFile%, Configuration, MirrorBackup, 0

; Read the console output setting.
IniRead, ConsoleOutput, %ConfigurationFile%, Configuration, ConsoleOutput, 1

; Read if the files/directories are logged.
IniRead, LogFilesDirs, %ConfigurationFile%, Configuration, LogFilesDirs, 1

; Read if only a file/directory list should be generated.
IniRead, ListOnly, %ConfigurationFile%, Configuration, ListOnly, 1

; Read if a backup job file should be generated.
IniRead, JobFile, %ConfigurationFile%, Configuration, JobFile, 0

; Read if the backup command should be displayed.
IniRead, ShowCommand, %ConfigurationFile%, Configuration, ShowCommand, 0

; Read if the own parameters should be used.
IniRead, OwnParameters, %ConfigurationFile%, Configuration, OwnParameters, 0

; Read the backup parameters.
IniRead, BackupParameters, %ConfigurationFile%, Configuration, BackupParameters


; ======================================================================
; P R E P A R E   P A T H   F I E L D S
; ======================================================================

; Check if the source path is empty.
If SourcePath =
{

  ; Specify the default source path text.
  SourcePath = Please select a source path ...

} ; Check if the source path is empty.

; Check if the destination path is empty.
If DestinationPath =
{

  ; Specify the default destination path text.
  DestinationPath = Please select a destination path ...

} ; Check if the destination path is empty.


; ======================================================================
; D I S P L A Y   T H E   G U I
; ======================================================================

; Specify the background color of the GUI.
Gui, Color, 004d9b

; Specify the fonts for the GUI (prefered font verdana).
Gui, Font, s8, MS sans serif
Gui, Font, s8, Arial
Gui, Font, s8, Verdana

; Add the gui background graphic.
Gui, Add, Picture, x0 y0, %A_ScriptDir%\image\PBcopy.jpg

; Add the logo.
Gui, Add, Picture, x10 y10 gWebsite, %A_ScriptDir%\image\Logo.gif

; Add the exit and start button text.
Gui, Add, Text, x400 y33 w60 h14 cWhite gExit Center, Exit
Gui, Add, Text, x400 y63 w60 h14 cWhite gBackup Center, Backup

; Add the source elements.
Gui, Add, Text, x28 y98 w90 h14 cWhite, Source Folder:
Gui, Add, Edit, x145 y95 w240 h20 vSource, %SourcePath%
Gui, Add, Button, x395 y95 w60 h20 gSource Center, Select

; Add the destination elements.
Gui, Add, Text, x28 y132 w115 h14 cWhite, Destination Folder:
Gui, Add, Edit, x145 y129 w240 h20 vDestination, %DestinationPath%
Gui, Add, Button, x395 y129 w60 h20 gDestination Center, Select

; Check if the mirror checkbox is selected.
If MirrorBackup = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y165 w105 h16 cWhite vMirrorBackupInput Checked, Mirror Backup

} ; Check if the mirror checkbox is selected.

; The mirror checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y165 w105 h16 cWhite vMirrorBackupInput, Mirror Backup

} ; The mirror checkbox is not selected.

; Check if the console output checkbox is selected.
If ConsoleOutput = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x156 y165 w115 h16 cWhite vConsoleOutputInput Checked, Console Output

} ; Check if the console output checkbox is selected.

; The console output checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x156 y165 w115 h16 cWhite vConsoleOutputInput, Console Output

} ; The console output checkbox is not selected.

; Check if the log files/dirs checkbox is selected.
If LogFilesDirs = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x293 y165 w165 h16 cWhite vLogFilesDirsInput Checked, Log Files and Directories

} ; Check if the log files/dirs checkbox is selected.

; The log files/dirs checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x293 y165 w165 h16 cWhite vLogFilesDirsInput, Log Files and Directories

} ; The log files/dirs checkbox is not selected.

; Check if the list only checkbox is selected.
If ListOnly = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y199 w75 h16 cWhite vListOnlyInput Checked, List Only

} ; Check if the list only checkbox is selected.

; The log list only checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y199 w75 h16 cWhite vListOnlyInput, List Only

} ; The log list only checkbox is not selected.

; Check the job file checkbox is selected.
If JobFile = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x156 y199 w120 h16 cWhite vJobFileInput Checked, Create a Job File

} ; Check the job file checkbox is selected.

; The job file checkbox is selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x156 y199 w120 h16 cWhite vJobFileInput, Create a Job File

} ; The job file checkbox is selected.

; Check if the show command checkbox is selected.
If ShowCommand = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x293 y199 w165 h16 cWhite vShowCommandInput Checked, Show Backup Command

} ; Check if the show command checkbox is selected.

; The show command checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x293 y199 w165 h16 cWhite vShowCommandInput, Show Backup Command

} ; The show command checkbox is not selected.

; Check if the own parameters checkbox is selected.
If OwnParameters = 1
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y233 w120 h16 cWhite vOwnParametersInput Checked, Use Parameters

} ; Check if the own parameters checkbox is selected.

; The own parameters checkbox is not selected.
else
{

  ; Add the checkbox.
  Gui, Add, CheckBox, x29 y233 w120 h16 cWhite vOwnParametersInput, Use Parameters

} ; The own parameters checkbox is not selected.

; Add the edit field for the backup parameter string.
Gui, Add, Edit, x154 y231 w300 h20 vBackupParametersInput, %BackupParameters%

; Display the gui.
Gui, Show, Center h300 w480, %ApplName% %ApplVersion% - Robocopy GUI

; End of the main script.
return


; ======================================================================
; S E L E C T   S O U R C E   P A T H
; ======================================================================

; Select the source path.
Source:

; Let the user choose a source path.
FileSelectFolder, SourcePath, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}, 0, Please select a source path !

; Check if the source path exist.
IfExist, %SourcePath%
{

  ; Write the path of the selected folder into the source input field.
  GuiControl,, Source, %SourcePath%

  ; Write the source path to the configuration file.
  IniWrite, %A_Space%%SourcePath%, %ConfigurationFile%, Configuration, SourcePath

} ; Check if the source path exist.

; The source path does not exist.
else
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, You did not select a valid source path ! Please try again !

  ; Go back to the select dialog.
  Goto Source

} ; The source path does not exist.

return


; ======================================================================
; S E L E C T   D E S T I N A T I O N   P A T H
; ======================================================================

; Select the destination path.
Destination:

; Let the user choose a destination path.
FileSelectFolder, DestinationPath, ::{20d04fe0-3aea-1069-a2d8-08002b30309d}, 0, Please select a destination path !

; Check if the destination path exist.
IfExist, %DestinationPath%
{

  ; Write the path of the selected folder into the destination input field.
  GuiControl,, Destination, %DestinationPath%

  ; Write the path of the destination path to the configuration file.
  IniWrite, %A_Space%%DestinationPath%, %ConfigurationFile%, Configuration, DestinationPath

} ; Check if the destination path exist.

; The destination path does not exist.
else
{

  ; Display an error message.
  MsgBox, 4112, %ApplName% %ApplVersion%, You did not select a valid destination path ! Please try again !

  ; Go back to the select dialog.
  Goto Destination

} ; The destination path does not exist.

return


; ======================================================================
; S T A R T   B A C K U P
; ======================================================================

; Start the backup.
Backup:

  ; Submit the values from the gui to the variables.
  Gui, Submit, NoHide

  ; Specify the mirror backup variable.
  MirrorBackup = %MirrorBackupInput%

  ; Specify the console output variable.
  ConsoleOutput = %ConsoleOutputInput%

  ; Specify the log files/dirs variable.
  LogFilesDirs = %LogFilesDirsInput%

  ; Specify the list only variable.
  ListOnly = %ListOnlyInput%

  ; Specify the job file variable.
  JobFile = %JobFileInput%

  ; Specify the show command variable.
  ShowCommand = %ShowCommandInput%

  ; Specify the own parameters variable.
  OwnParameters = %OwnParametersInput%

  ; Specify the backup parameters variable.
  BackupParameters = %BackupParametersInput%


  ; ====================================================================
  ; W R I T E   S E T T I N G S   T O   C O N F I G   F I L E
  ; ====================================================================

  ; Write the mirror backup variable to the configuration file.
  IniWrite, %A_Space%%MirrorBackup%, %ConfigurationFile%, Configuration, MirrorBackup

  ; Write the console output variable to the configuration file.
  IniWrite, %A_Space%%ConsoleOutput%, %ConfigurationFile%, Configuration, ConsoleOutput

  ; Write the log files/dirs variable to the configuration file.
  IniWrite, %A_Space%%LogFilesDirs%, %ConfigurationFile%, Configuration, LogFilesDirs

  ; Write the list only variable to the configuration file.
  IniWrite, %A_Space%%ListOnly%, %ConfigurationFile%, Configuration, ListOnly

  ; Write the job file variable to the configuration file.
  IniWrite, %A_Space%%JobFile%, %ConfigurationFile%, Configuration, JobFile

  ; Write the show command variable to the configuration file.
  IniWrite, %A_Space%%ShowCommand%, %ConfigurationFile%, Configuration, ShowCommand

  ; Write the own parameters variable to the configuration file.
  IniWrite, %A_Space%%OwnParameters%, %ConfigurationFile%, Configuration, OwnParameters

  ; Write the backup parameters variable to the configuration file.
  IniWrite, %A_Space%%BackupParameters%, %ConfigurationFile%, Configuration, BackupParameters


  ; ====================================================================
  ; S P E C I F Y   T H E   R O B O C O P Y   O P T I O N S
  ; ====================================================================

  ; Check if the source path exist.
  IfExist, %SourcePath%
  {

    ; Check if the destination path exist.
    IfExist, %DestinationPath%
    {

      ; Check if the source and destination path are not the same.
      If SourcePath != %DestinationPath%
      {

        ; Initialize the start backup variable.
        StartBackup := 1

        ; Specify the backup command.
        BackupCommand := "Robocopy.exe """ . SourcePath . """ """ . DestinationPath . """ /LOG:" . LogFile

        ; Check if the standard backup command will be used.
        If OwnParameters != 1
        {

          ; Specify the default backup parameters.
          BackupParameters := "/ZB /COPY:DAT /R:10 /W:30 /NP"

          ; Check if the mirror backup is enabled.
          If MirrorBackup = 1
          {

            ; Add the mirror backup option to the backup parameters.
            BackupParameters := BackupParameters . " /MIR"

          } ; Check if the mirror backup is enabled.

          ; The mirror backup is disabled.
          else
          {

            ; Add the copy directories option to the backup parameters.
            BackupParameters := BackupParameters . " /E"

          } ; The mirror backup is disabled.

          ; Check if the console output is enabled.
          If ConsoleOutput = 1
          {

            ; Add the console output option to the backup parameters.
            BackupParameters := BackupParameters . " /TEE"

          } ; Check if the console output is enabled.

          ; Check if the log files/dirs option is enabled.
          If LogFilesDirs != 1
          {

            ; Add the log files/dirs option to the backup parameters.
            BackupParameters := BackupParameters . " /NFL /NDL"

          } ; Check if the log files/dirs option is enabled.

          ; Check if the list only option is enabled.
          If ListOnly = 1
          {

            ; Add the list only option to the backup parameters.
            BackupParameters := BackupParameters . " /L"

          } ; Check if the list only option is enabled.

          ; Check if the job file option is enabled.
          If JobFile = 1
          {

            ; Add the job file option to the backup parameters.
            BackupParameters := BackupParameters . " /SAVE:ROBOCOPY_JOB.RCJ"

          } ; Check if the job file option is enabled.

        } ; Check if the standard backup command will be used.

        ; The own backup command will be used.
        else
        {

          ; Check if the own backup parameters are not empty.
          If BackupParameters =
          {

            ; Display an error message.
            MsgBox, 4112, %ApplName% %ApplVersion%, You did not specify any backup parameters !

            ; Set the start backup variable to 0.
            StartBackup = 0

          } ; Check if the own backup parameters are not empty.

        } ; The own backup command will be used.

        ; Check if the backup command should be displayed.
        If ShowCommand = 1
        {

          ; Show an information message.
          MsgBox, 4160, %ApplName% %ApplVersion% - Information, Command: %BackupCommand% %BackupParameters%

          ; Copy the backup command to the clipboard.
          clipboard = %BackupCommand% %BackupParameters%

        } ; Check if the backup command should be displayed.

        ; Check if the backup should be started.
        If StartBackup = 1
        {

          ; Minimize the PBcopy window.
          WinMinimize, %ApplName% %ApplVersion% - Robocopy GUI

          ; Run the backup.
          RunWait, %BackupCommand% %BackupParameters%

          ; Display an information message.
          MsgBox, 4160, %ApplName% %ApplVersion% - Information, The backup has finished ! Please check the logfile !

          ; Restore the PBcopy window.
          WinRestore, %ApplName% %ApplVersion% - Robocopy GUI

          ; Check if the logfile exist.
          IfExist, %LogFile%
          {

            ; Show the logfile.
            Run, %LogFile%

          } ; Check if the logfile exist.

        } ; Check if the backup should be started.

      } ; Check if the source and destination path are not the same.

      ; The source and destination path are the same.
      else
      {

        ; Display an error message.
        MsgBox, 4112, %ApplName% %ApplVersion%, The source and destination directories are the same !

      } ; The source and destination path are the same.

    } ; Check if the destination path exist.

    ; The destination path does not exist.
    else
    {

      ; Display an error message.
      MsgBox, 4112, %ApplName% %ApplVersion%, The destination path '%DestinationPath%' does not exist !

    } ; The destination path does not exist.

  } ; Check if the source path exist.

  ; The source path does not exist.
  else
  {

    ; Display an error message.
    MsgBox, 4112, %ApplName% %ApplVersion%, The source path '%SourcePath%' does not exist !

  } ; The source path does not exist.

return


; ======================================================================
; O P E N   T H E   C O M P A N Y   W E B S I T E
; ======================================================================

; Open the company website.
Website:

  ; Open the PB-Soft website.
  Run, %ApplWebsite%, %A_ScriptDir%, UseErrorLevel

  ; Check if the website could not be opened.
  if ErrorLevel = ERROR
  {

    ; Display an error message.
    MsgBox, 4112, %ApplName% %ApplVersion%, There is no default application specified for opening websites !

  } ; Check if the website could not be opened.

return


; ======================================================================
; E X I T   T H E   P B C O P Y   A P P L I C A T I O N
; ======================================================================

; Exit the script.
GuiClose:
Exit:

  ; Display an exit question.
  MsgBox, 4132, %ApplName% %ApplVersion%, Do you really want to exit ?

  ; Check if the yes button was pressed.
  IfMsgBox Yes
  {

    ; Exit the application.
    ExitApp

  } ; Check if the yes button was pressed.

return


; ======================================================================
; O P E N   T H E   C O N F I G U R A T I O N   F I L E
; ======================================================================

; Specify the hotkey to show the configuration file - CTRL + ALT + C
^!c::

  ; Check if the configuration file exist.
  IfExist, %ConfigurationFile%
  {

    ; Open the configuration file.
    Run, %ConfigurationFile%

  } ; Check if the configuration file exist.

  ; There is no configuration file available.
  else
  {

    ; Display an error message.
    MsgBox, 4112, %ApplName% %ApplVersion%, There is no configuration file available !

  } ; There is no configuration file available.

return


; ======================================================================
; R E S T A R T   T H E   A P P L I C A T I O N
; ======================================================================

; Specify the hotkey to reload the application - CTRL + ALT + R
^!r::

  ; Restart the application.
  Reload

return


; ======================================================================
; S H O W   T H E   S H O R T C U T   W I N D O W
; ======================================================================

; Specify the hotkey to show the shortcut window - CTRL + ALT + S
^!s::

  ; Specify the information text.
  InfoText = The following shortcuts are available:`n`n

  ; Add all shortcut combinations.
  InfoText = %InfoText%CTRL + ALT + C   -->  Open the configuration file`n
  InfoText = %InfoText%CTRL + ALT + R   -->  Reload the application`n
  InfoText = %InfoText%CTRL + ALT + S   -->  Show this shortcut window`n
  InfoText = %InfoText%CTRL + ALT + V   -->  Display the application version`n
  InfoText = %InfoText%CTRL + ALT + W  -->  Display the PB-Soft website`n
  InfoText = %InfoText%CTRL + ALT + X   -->  Exit the application`n`n

  ; Display an information message.
  MsgBox, 4160, %ApplName% %ApplVersion%, %InfoText%

return


; ======================================================================
; D I S P L A Y   T H E   A P P L I C A T I O N   V E R S I O N
; ======================================================================

; Specify the hotkey to show the application version - CTRL + ALT + V
^!v::

  ; Display the application version.
  MsgBox, 4160, %ApplName% %ApplVersion%, Application Name: %ApplName%`n`nApplication Version: %ApplVersion%`n`nInternal Version: %ApplIntVersion%`n`nApplication Build Date: %ApplBuildDate%`n`nWebsite: %ApplWebsite%`n`n%Copyright%

return


; ======================================================================
; O P E N   T H E   C O M P A N Y   W E B S I T E
; ======================================================================

; Specify the hotkey to show the company website - CTRL + ALT + W
^!w::

  ; Open the company website.
  Gosub, Website

return


; ======================================================================
; E X I T   T H E   C H E C K N E T   A P P L I C A T I O N
; ======================================================================

; Specify the hotkey to exit the application - CTRL + ALT + X
^!x::

  ; Exit the application.
  Gosub, Exit

return
