# PBcopy - README #
---

### Overview ###

The **PBcopy** tool is a graphical user interface (GUI) for the **Robocopy** tool from **Microsoft**. Please download the **Windows Server 2003 Resource Kit Tools** which contain the **Robocopy Executable (Version XP010)** from the following website:

Download: [**Windows Server 2003 Resource Kit Tools**](https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=17657)

### Screenshots ###

![PBcopy - User Interface](development/readme/pbcopy1.png "PBcopy - User Interface")

![PBcopy - Shortcuts](development/readme/pbcopy2.png "PBcopy - Shortcuts")

### Setup ###

* Copy the directory **pbcopy** to your PC.
* Download the [**Windows Server 2003 Resource Kit Tools**](https://www.microsoft.com/en-us/download/details.aspx?displaylang=en&id=17657).
* Copy the **Robocopy Executable** to the directory **pbcopy**.
* Connect the backup drives (internal or external harddisks, flashdrives, etc...) to your PC.
* Start the **PBcopy.exe** executable with a doubleclick.
* Select the source and destination directories for your backup.
* Select the necessary options for your backup.
* Press the button **Backup** to start your backup.
* You can press **CTRL + ALT + S** to display all the available shortcuts.
* **WARNING: Please check that the source and destination directories are specified correctly before starting the backup !!!!**


### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBcopy** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
